function sky(){
    var STARSNUM=50
    var SPEED=1.5
    var SIZEMIN=2
    var SIZEMAX=5-(SIZEMIN+1)

    var windowWidth=window.innerWidth
    var windowHeight=window.innerHeight
    var sky=document.getElementById('sky')

    for(var i=0; i<STARSNUM; i++){
        var size = Math.floor((Math.random()*SIZEMAX)+SIZEMIN)
        var animDur = Math.floor((Math.random()*10+4)/(SPEED/2))
        var posX = Math.floor((Math.random()*100)+1)
        var posY = Math.floor((Math.random()*100)+1)

        var style='width: '+size+'px; height: '+size+'px; left: '+posX+'%; top: '+posY+'%;'+'animation-duration: '+animDur+'s;'

        var div=document.createElement("DIV")

        div.setAttribute("class","star")
        div.setAttribute("style",style)
        sky.appendChild(div)
    }
}



/*******************************************************************************
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
/////////        ///   ////////     ////////  ///    ///////         //      ///
////////   //////////   //////  //  ///////  ///  //  //////  /////  ////  /////
////////   ///////////   ////  ////  /////  ///  ////  /////  /////  ////  /////
////////   ///////////   ///  //////  ///  ///          ////         ////  /////
////////   ////////////   /  ////////  /  ///  ////////  ///  ///////////  /////
///       /////////////     //////////   ///  //////////  //  /////////      ///
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
*******************************************************************************/

function Swapi(idCategorySelect,idResultSelect){
    this.idCategory=document.getElementById(idCategorySelect)
    this.idResult=document.getElementById(idResultSelect)

    var url
    var index=0
    var json
    var data={}
    var count=1
    var operating=false
    var operatingResource=false


    this.addResource=function(exist=false){
        if(operating===false){
            operating=true
            if(json==null){
                this.idResult.parentElement.hidden=true
                url=this.idCategory.value
                index=this.idCategory.selectedIndex


                if(data['name']=this.nameOf(url)){
                    if(data[data['name']]==null){
                        console.log('START : Téléchargement de '+data['name'])
                        operating=false
                        this.requestSwapi()
                    }else{
                        console.log('START : Chargement de '+data['name'])
                        operating=false
                        this.changeResource()
                    }
                }else{
                    operating=false
                    return 'ERROR : la ressource donnée n\'est pas acceptable'
                }
            }else{
                var type;
                if(json.results[0].name!=null){
                    type='name'
                }else if(json.results[0].title!=null){
                    type='title'
                }else{
                    operating=false
                    return 'ERROR : la ressource reçu a un défaut'
                }

                if(operatingResource===false){
                    for(var i=this.idResult.children.length-1;i>=0;i--){
                        this.idResult.children[i].remove()
                    }
                    var option=document.createElement("OPTION")
                    var text=document.createTextNode(data['name'])

                    option.appendChild(text)
                    option.setAttribute("VALUE",'null')
                    this.idResult.appendChild(option)
                }

                if(data[data['name']]==null){
                    data[data['name']]=[]
                }

                for(var i=0;i<json.results.length;i++){
                    var option=document.createElement("OPTION")
                    var text=document.createTextNode(count+' '+json.results[i][type])

                    if(exist===false){
                        data[data['name']].push(json.results[i])
                    }

                    option.appendChild(text)
                    option.setAttribute("VALUE",count)
                    this.idResult.appendChild(option)
                    count++
                }


                if(json.next!=null){
                    url=json.next
                    operatingResource=true
                    operating=false
                    this.requestSwapi()
                }else{
                    if(exist===false){
                        console.log('END : Téléchargement de '+data['name'])
                    }else{
                        console.log('END : Chargement de '+data['name'])
                    }

                    count=1
                    json=undefined
                    url=undefined
                    this.idResult.parentElement.hidden=false
                    operatingResource=false
                    operating=false
                }
            }
        }else{
            this.idCategory.selectedIndex=index
        }
    }



    this.requestSwapi=function(){
        if(operating===false){
            operating=true
            if(url==''||url==null){
                operating=false
                return false
            }
            var xhttp=new XMLHttpRequest()

            xhttp.myObject=this
            xhttp.onreadystatechange=function(){
                if(this.readyState==4&&this.status==200){
                    json=JSON.parse(this.response)
                    operating=false
                    this.myObject.addResource()
                }else if(this.readyState==4){
                    operating=false
                }
            }

            xhttp.open("GET",url,true)
            xhttp.send()
        }
    }



    this.changeResource=function(){
        if(operating===false){
            operating=true
            json={}
            json.results=data[data['name']]
            json.next=null
            operating=false
            this.addResource(true)
        }
    }




    this.nameOf=function(data){
        for(var i=data.length-2;i>=0;i--){
            if(data[i]==='/'){
                return data.substring(i+1,data.length-1)
            }
        }
        return false
    }
}
