<!DOCTYPE html>
<html lang="fr" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title>StarWarasme - <?php echo ucfirst($_GET['page']) ?></title>
        <link rel="stylesheet" href="main.css">
        <script src="main.js" charset="utf-8"></script>
    </head>
    <body>
        <div id="sky"></div>
<?php if($_GET['page']!=='profil') require_once 'add/header.php' ?>
        <main class="main">
<?php
            if(!include_once 'add/'.$_GET['page'].'.php'){
                header('location: /accueil');
                echo "<script>window.location.assign('/accueil')</script>";
            }
?>
        </main>
    </body>
</html>
